'use strict';

angular.module('todo').factory('Todo', [
	function() {
		// Todo service logic
		// ...

		// Public API
		return {
			someMethod: function() {
				return true;
			}
		};
	}
]);