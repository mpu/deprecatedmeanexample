'use strict';

angular.module('todo').directive('todo', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Todo directive logic
				// ...

				element.text('this is the todo directive');
			}
		};
	}
]);