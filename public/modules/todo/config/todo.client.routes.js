'use strict';

//Setting up route
angular.module('todo').config(['$stateProvider',
	function($stateProvider) {
		// Todo state routing
		$stateProvider.
		state('todo', {
			url: '/todo',
			templateUrl: 'modules/todo/views/todo.client.view.html'
		}).
		state('createtodo',{
			url: '/todo/createtodo',
			templateUrl: 'modules/todo/views/create-todo.client.view.html'
		});
	}
]);