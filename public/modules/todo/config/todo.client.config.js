'use strict';

// Todo module config
angular.module('todo').run(['Menus',
	function(Menus) {
		/*
		* Notice here the official documentation is not clear
		http://meanjs.org/docs.html#menus
		here is the solution https://groups.google.com/forum/#!topic/meanjs/QFrlXGs9Ue8
		*/
		Menus.addMenuItem('topbar', 'Todos', 'todos', 'dropdown', '/todo(/create)?');
		Menus.addSubMenuItem('topbar', 'todos', 'List Todos', 'todo');
		Menus.addSubMenuItem('topbar', 'todos', 'New Todo', 'todo/createtodo');
	}
]);