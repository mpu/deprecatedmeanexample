'use strict';

angular.module('todo').controller('TodoControllerController', ['$scope',
	function($scope,$location,Todo) {
		$scope.create = function() {
			// Create new Mongocrud object
			var todo = new Todo ({
				name: this.name
			});

			// Redirect after save
			todo.$save(function(response) {
				$location.path('todo/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
	}
]);