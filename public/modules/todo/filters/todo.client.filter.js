'use strict';

angular.module('todo').filter('todo', [
	function() {
		return function(input) {
			// Todo directive logic
			// ...

			return 'todo filter: ' + input;
		};
	}
]);