'use strict';

//Setting up route
angular.module('mongocruds').config(['$stateProvider',
	function($stateProvider) {
		// Mongocruds state routing
		$stateProvider.
		state('listMongocruds', {
			url: '/mongocruds',
			templateUrl: 'modules/mongocruds/views/list-mongocruds.client.view.html'
		}).
		state('createMongocrud', {
			url: '/mongocruds/create',
			templateUrl: 'modules/mongocruds/views/create-mongocrud.client.view.html'
		}).
		state('viewMongocrud', {
			url: '/mongocruds/:mongocrudId',
			templateUrl: 'modules/mongocruds/views/view-mongocrud.client.view.html'
		}).
		state('editMongocrud', {
			url: '/mongocruds/:mongocrudId/edit',
			templateUrl: 'modules/mongocruds/views/edit-mongocrud.client.view.html'
		});
	}
]);