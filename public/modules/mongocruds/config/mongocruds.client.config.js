'use strict';

// Configuring the Articles module
angular.module('mongocruds').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Mongocruds', 'mongocruds', 'dropdown', '/mongocruds(/create)?');
		Menus.addSubMenuItem('topbar', 'mongocruds', 'List Mongocruds', 'mongocruds');
		Menus.addSubMenuItem('topbar', 'mongocruds', 'New Mongocrud', 'mongocruds/create');
	}
]);