'use strict';

(function() {
	// Mongocruds Controller Spec
	describe('Mongocruds Controller Tests', function() {
		// Initialize global variables
		var MongocrudsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Mongocruds controller.
			MongocrudsController = $controller('MongocrudsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Mongocrud object fetched from XHR', inject(function(Mongocruds) {
			// Create sample Mongocrud using the Mongocruds service
			var sampleMongocrud = new Mongocruds({
				name: 'New Mongocrud'
			});

			// Create a sample Mongocruds array that includes the new Mongocrud
			var sampleMongocruds = [sampleMongocrud];

			// Set GET response
			$httpBackend.expectGET('mongocruds').respond(sampleMongocruds);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.mongocruds).toEqualData(sampleMongocruds);
		}));

		it('$scope.findOne() should create an array with one Mongocrud object fetched from XHR using a mongocrudId URL parameter', inject(function(Mongocruds) {
			// Define a sample Mongocrud object
			var sampleMongocrud = new Mongocruds({
				name: 'New Mongocrud'
			});

			// Set the URL parameter
			$stateParams.mongocrudId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/mongocruds\/([0-9a-fA-F]{24})$/).respond(sampleMongocrud);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.mongocrud).toEqualData(sampleMongocrud);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Mongocruds) {
			// Create a sample Mongocrud object
			var sampleMongocrudPostData = new Mongocruds({
				name: 'New Mongocrud'
			});

			// Create a sample Mongocrud response
			var sampleMongocrudResponse = new Mongocruds({
				_id: '525cf20451979dea2c000001',
				name: 'New Mongocrud'
			});

			// Fixture mock form input values
			scope.name = 'New Mongocrud';

			// Set POST response
			$httpBackend.expectPOST('mongocruds', sampleMongocrudPostData).respond(sampleMongocrudResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Mongocrud was created
			expect($location.path()).toBe('/mongocruds/' + sampleMongocrudResponse._id);
		}));

		it('$scope.update() should update a valid Mongocrud', inject(function(Mongocruds) {
			// Define a sample Mongocrud put data
			var sampleMongocrudPutData = new Mongocruds({
				_id: '525cf20451979dea2c000001',
				name: 'New Mongocrud'
			});

			// Mock Mongocrud in scope
			scope.mongocrud = sampleMongocrudPutData;

			// Set PUT response
			$httpBackend.expectPUT(/mongocruds\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/mongocruds/' + sampleMongocrudPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid mongocrudId and remove the Mongocrud from the scope', inject(function(Mongocruds) {
			// Create new Mongocrud object
			var sampleMongocrud = new Mongocruds({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Mongocruds array and include the Mongocrud
			scope.mongocruds = [sampleMongocrud];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/mongocruds\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleMongocrud);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.mongocruds.length).toBe(0);
		}));
	});
}());