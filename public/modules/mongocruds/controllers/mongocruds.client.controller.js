'use strict';

// Mongocruds controller
angular.module('mongocruds').controller('MongocrudsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Mongocruds',
	function($scope, $stateParams, $location, Authentication, Mongocruds) {
		$scope.authentication = Authentication;

		// Create new Mongocrud
		$scope.create = function() {
			// Create new Mongocrud object
			var mongocrud = new Mongocruds ({
				name: this.name
			});

			// Redirect after save
			mongocrud.$save(function(response) {
				$location.path('mongocruds/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Mongocrud
		$scope.remove = function(mongocrud) {
			if ( mongocrud ) { 
				mongocrud.$remove();

				for (var i in $scope.mongocruds) {
					if ($scope.mongocruds [i] === mongocrud) {
						$scope.mongocruds.splice(i, 1);
					}
				}
			} else {
				$scope.mongocrud.$remove(function() {
					$location.path('mongocruds');
				});
			}
		};

		// Update existing Mongocrud
		$scope.update = function() {
			var mongocrud = $scope.mongocrud;

			mongocrud.$update(function() {
				$location.path('mongocruds/' + mongocrud._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Mongocruds
		$scope.find = function() {
			$scope.mongocruds = Mongocruds.query();
		};

		// Find existing Mongocrud
		$scope.findOne = function() {
			$scope.mongocrud = Mongocruds.get({ 
				mongocrudId: $stateParams.mongocrudId
			});
		};
	}
]);