'use strict';

//Mongocruds service used to communicate Mongocruds REST endpoints
angular.module('mongocruds').factory('Mongocruds', ['$resource',
	function($resource) {
		return $resource('mongocruds/:mongocrudId', { mongocrudId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);