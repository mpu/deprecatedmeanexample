'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Mongocrud = mongoose.model('Mongocrud'),
	_ = require('lodash');

/**
 * Create a Mongocrud
 */
exports.create = function(req, res) {
	var mongocrud = new Mongocrud(req.body);
	mongocrud.user = req.user;

	mongocrud.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(mongocrud);
		}
	});
};

/**
 * Show the current Mongocrud
 */
exports.read = function(req, res) {
	res.jsonp(req.mongocrud);
};

/**
 * Update a Mongocrud
 */
exports.update = function(req, res) {
	var mongocrud = req.mongocrud ;

	mongocrud = _.extend(mongocrud , req.body);

	mongocrud.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(mongocrud);
		}
	});
};

/**
 * Delete an Mongocrud
 */
exports.delete = function(req, res) {
	var mongocrud = req.mongocrud ;

	mongocrud.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(mongocrud);
		}
	});
};

/**
 * List of Mongocruds
 */
exports.list = function(req, res) { 
	Mongocrud.find().sort('-created').populate('user', 'displayName').exec(function(err, mongocruds) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(mongocruds);
		}
	});
};

/**
 * Mongocrud middleware
 */
exports.mongocrudByID = function(req, res, next, id) { 
	Mongocrud.findById(id).populate('user', 'displayName').exec(function(err, mongocrud) {
		if (err) return next(err);
		if (! mongocrud) return next(new Error('Failed to load Mongocrud ' + id));
		req.mongocrud = mongocrud ;
		next();
	});
};

/**
 * Mongocrud authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.mongocrud.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
