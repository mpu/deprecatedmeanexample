'use strict';

var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Todo = mongoose.model('Todo'),
	_ = require('lodash');

/**
 * Create a todo
 */
exports.create = function(req, res) {
	var todo = new Todo(req.body);
	todo.user = req.user;

	todo.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(todo);
		}
	});
};

/**
 * Show the current todo
 */
exports.read = function(req, res) {
	console.log('hi');
	res.json(req.todo);
};

/**
 * Update a todo
 */
exports.update = function(req, res) {
	var todo = req.todo;

	todo = _.extend(todo, req.body);

	todo.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(todo);
		}
	});
};

/**
 * Delete an todo
 */
exports.delete = function(req, res) {
	var todo = req.todo;

	todo.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(todo);
		}
	});
};

exports.list = function(req, res) {
	var todo = req.todo;
	
	todo.find().sort('-created').populate('user', 'displayName').exec(function(err, todo) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(todo);
		}
	});
};

/**
 * Article middleware
 */
exports.articleByID = function(req, res, next, id) {
	Todo.findById(id).populate('user', 'displayName').exec(function(err, todo) {
		if (err) return next(err);
		if (!todo) return next(new Error('Failed to load todo ' + id));
		req.todo = todo;
		next();
	});
};

/**
 * Article authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.todo.user.id !== req.user.id) {
		return res.status(403).send({
			message: 'User is not authorized'
		});
	}
	next();
};
