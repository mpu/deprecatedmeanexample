'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var mongocruds = require('../../app/controllers/mongocruds.server.controller');

	// Mongocruds Routes
	app.route('/mongocruds')
		.get(mongocruds.list)
		.post(users.requiresLogin, mongocruds.create);

	app.route('/mongocruds/:mongocrudId')
		.get(mongocruds.read)
		.put(users.requiresLogin, mongocruds.hasAuthorization, mongocruds.update)
		.delete(users.requiresLogin, mongocruds.hasAuthorization, mongocruds.delete);

	// Finish by binding the Mongocrud middleware
	app.param('mongocrudId', mongocruds.mongocrudByID);
};
