'use strict';

var todos= require('../../app/controllers/todo.server.controller'),
	users = require('../../app/controllers/users.server.controller');

module.exports=function(app){
	app.route('/todo')
		.get(todos.list)
		.post(users.requiresLogin, todos.create);

	app.route('/todo/:todoId')
		.get(todos.read)
		.put(users.requiresLogin, todos.hasAuthorization, todos.update)
		.delete(users.requiresLogin, todos.hasAuthorization, todos.delete);

	app.param('todoId', todos.articleByID);
};