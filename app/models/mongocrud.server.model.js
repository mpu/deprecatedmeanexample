'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Mongocrud Schema
 */
var MongocrudSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Mongocrud name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Mongocrud', MongocrudSchema);