'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Mongocrud = mongoose.model('Mongocrud'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, mongocrud;

/**
 * Mongocrud routes tests
 */
describe('Mongocrud CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Mongocrud
		user.save(function() {
			mongocrud = {
				name: 'Mongocrud Name'
			};

			done();
		});
	});

	it('should be able to save Mongocrud instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Mongocrud
				agent.post('/mongocruds')
					.send(mongocrud)
					.expect(200)
					.end(function(mongocrudSaveErr, mongocrudSaveRes) {
						// Handle Mongocrud save error
						if (mongocrudSaveErr) done(mongocrudSaveErr);

						// Get a list of Mongocruds
						agent.get('/mongocruds')
							.end(function(mongocrudsGetErr, mongocrudsGetRes) {
								// Handle Mongocrud save error
								if (mongocrudsGetErr) done(mongocrudsGetErr);

								// Get Mongocruds list
								var mongocruds = mongocrudsGetRes.body;

								// Set assertions
								(mongocruds[0].user._id).should.equal(userId);
								(mongocruds[0].name).should.match('Mongocrud Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Mongocrud instance if not logged in', function(done) {
		agent.post('/mongocruds')
			.send(mongocrud)
			.expect(401)
			.end(function(mongocrudSaveErr, mongocrudSaveRes) {
				// Call the assertion callback
				done(mongocrudSaveErr);
			});
	});

	it('should not be able to save Mongocrud instance if no name is provided', function(done) {
		// Invalidate name field
		mongocrud.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Mongocrud
				agent.post('/mongocruds')
					.send(mongocrud)
					.expect(400)
					.end(function(mongocrudSaveErr, mongocrudSaveRes) {
						// Set message assertion
						(mongocrudSaveRes.body.message).should.match('Please fill Mongocrud name');
						
						// Handle Mongocrud save error
						done(mongocrudSaveErr);
					});
			});
	});

	it('should be able to update Mongocrud instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Mongocrud
				agent.post('/mongocruds')
					.send(mongocrud)
					.expect(200)
					.end(function(mongocrudSaveErr, mongocrudSaveRes) {
						// Handle Mongocrud save error
						if (mongocrudSaveErr) done(mongocrudSaveErr);

						// Update Mongocrud name
						mongocrud.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Mongocrud
						agent.put('/mongocruds/' + mongocrudSaveRes.body._id)
							.send(mongocrud)
							.expect(200)
							.end(function(mongocrudUpdateErr, mongocrudUpdateRes) {
								// Handle Mongocrud update error
								if (mongocrudUpdateErr) done(mongocrudUpdateErr);

								// Set assertions
								(mongocrudUpdateRes.body._id).should.equal(mongocrudSaveRes.body._id);
								(mongocrudUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Mongocruds if not signed in', function(done) {
		// Create new Mongocrud model instance
		var mongocrudObj = new Mongocrud(mongocrud);

		// Save the Mongocrud
		mongocrudObj.save(function() {
			// Request Mongocruds
			request(app).get('/mongocruds')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Mongocrud if not signed in', function(done) {
		// Create new Mongocrud model instance
		var mongocrudObj = new Mongocrud(mongocrud);

		// Save the Mongocrud
		mongocrudObj.save(function() {
			request(app).get('/mongocruds/' + mongocrudObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', mongocrud.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Mongocrud instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Mongocrud
				agent.post('/mongocruds')
					.send(mongocrud)
					.expect(200)
					.end(function(mongocrudSaveErr, mongocrudSaveRes) {
						// Handle Mongocrud save error
						if (mongocrudSaveErr) done(mongocrudSaveErr);

						// Delete existing Mongocrud
						agent.delete('/mongocruds/' + mongocrudSaveRes.body._id)
							.send(mongocrud)
							.expect(200)
							.end(function(mongocrudDeleteErr, mongocrudDeleteRes) {
								// Handle Mongocrud error error
								if (mongocrudDeleteErr) done(mongocrudDeleteErr);

								// Set assertions
								(mongocrudDeleteRes.body._id).should.equal(mongocrudSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Mongocrud instance if not signed in', function(done) {
		// Set Mongocrud user 
		mongocrud.user = user;

		// Create new Mongocrud model instance
		var mongocrudObj = new Mongocrud(mongocrud);

		// Save the Mongocrud
		mongocrudObj.save(function() {
			// Try deleting Mongocrud
			request(app).delete('/mongocruds/' + mongocrudObj._id)
			.expect(401)
			.end(function(mongocrudDeleteErr, mongocrudDeleteRes) {
				// Set message assertion
				(mongocrudDeleteRes.body.message).should.match('User is not logged in');

				// Handle Mongocrud error error
				done(mongocrudDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Mongocrud.remove().exec();
		done();
	});
});